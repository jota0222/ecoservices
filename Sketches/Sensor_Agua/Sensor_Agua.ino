#include <SPI.h>
#include <WiFi.h>
#include <SD.h>

#define TIEMPO 1000
#define FACTOR_CONVERSION 7.11; // Para convertir de frecuencia a caudal
#define PIN_SENSOR 2    // Sensor conectado en el pin 2
#define ID 1

char ssid[] = "yourNetwork"; // Nombre de Red
char pass[] = "secretPassword"; // Contraseña de red
int keyIndex = 0;            // your network key Index number (needed only for WEP)

// byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }; // MAC para el dispositivo
// IPAddress ip(192, 168, 0, 177); // IP estática en caso de que falle el DHCP
IPAddress server(192, 168, 0, 103); // IP de servidor local de pruebas, descomentar para pruebas
//char server[] = "www.ecoservices.com.co"; // Descomentar para usar DNS
//EthernetClient client;
WiFiClient client;
bool connectionError = false;

volatile int NumPulsos; // Variable para la cantidad de pulsos recibidos

float volumen = 0;
float m3;

long dt = 0; //variación de tiempo por cada bucle
long t0 = 0; //millis() del bucle anterior

int fileCount = 0;


void setup()
{
  //  Serial.begin(9600);
  //
  //  Serial.print("Iniciando Ethernet ...");
  int status = WL_IDLE_STATUS;
  while (status != WL_CONNECTED) {
    // Serial.println("Falló Ethernet usando DHCP");
    // try to congifure using IP address instead of DHCP:
    status = WiFi.begin(ssid, pass);
    delay(10000);
  }

  //  Serial.print("Iniciando SD ...");
  if (!SD.begin(4)) {
    //    Serial.println("No se pudo inicializar SD");
    return;
  }
  //  Serial.println("inicializacion SD exitosa");

  pinMode(PIN_SENSOR, INPUT);
  attachInterrupt(0, ContarPulsos, RISING);//(Interrupción 0(Pin2),función,Flanco de subida)

  //  Serial.println ("Envie '1' para restablecer el volumen a 0 Litros");

  t0 = millis();
}


void loop()
{
  double caudal_L_m = ObtenerFrecuecia() / FACTOR_CONVERSION; //obtenemos la frecuencia de los pulsos en Hz y calculamos el caudal en L/m

  dt = millis() - t0; //calculamos la variación de tiempo
  t0 = millis();

  volumen = (caudal_L_m / 60) * (dt / 1000); // volumen(L)=caudal(L/s)*tiempo(s) - No se acumula. El acumulado lo hace el servidor.
  m3 = volumen / 1000; // Conversión a metros cúbicos (m^3)

  //  if (Serial.available()) {
  //    if(Serial.read()=='1') volumen = 0;//restablecemos el volumen si recibimos 'r'
  //  }

  //----- Enviamos por el puerto serie -----//
  //  Serial.print("metros cubicos : ");
  //  Serial.print(m3, 3);
  //  Serial.print ("  Caudal: ");
  //  Serial.print (caudal_L_m, 3);
  //  Serial.print (" L/mint Volumen: ");
  //  Serial.print (volumen, 3);
  //  Serial.println (" L");

  String fileString = "datos";

  if (fileCount % 5 == 0) { // Si hay más de 5 archivos creados, borrar el primero
    SD.remove(fileString + (fileCount - 5));
  }

  //----- Escribimos en archivo en caso de que no se pueda realizar la conexión -----//
  File data = SD.open(fileString + fileCount, FILE_WRITE);//abrimos  el archivo
  if (data) {
    //    Serial.print("Escribiendo SD: ");

    // Si se crea en JSON
    //    data.print("{Volume:");
    //    data.print(m3);
    //    data.print(",LastTime:");
    //    data.print(t0);
    //    data.print("},");

    // Si se crea en CSV
    data.print(m3, 3);
    data.print(';');
    data.println(t0, 3);
  }
  //  else {
  //    Serial.println("Error al abrir el archivo");
  //  }

  //----- Enviamos información al servidor -----//
  if (!connectionError) {
    if (data) {
      data.close(); //cerramos el archivo
    }
    connectionError = !SendVol(m3);
  } else {
    if (data) {
      connectionError = !SendFile(data);
      data.close(); //cerramos el archivo
    }
  }

  //delay(1000);
}

//----- Función que se ejecuta en interrupción -----//
void ContarPulsos ()
{
  NumPulsos++;  //incrementamos la variable de pulsos
}

//----- Función para obtener frecuencia de los pulsos -----//
int ObtenerFrecuecia()
{
  NumPulsos = 0;   // Ponemos a 0 el número de pulsos

  interrupts();    // Habilitamos las interrupciones
  delay(TIEMPO);   // Muestra de 1 segundo
  noInterrupts(); // Deshabilitamos  las interrupciones

  return NumPulsos; // Hz(pulsos por segundo);
}

//----- Enviar información al servidor dato por dato -----//
bool SendVol(float vol) {
  if (client.connect(server, 80)) {
    //    Serial.println("connected");
    // realizando una petición HTTP:
    String getString = "GET /sensores/agua?id=";
    getString = getString + ID + "&vol=" + vol + "&t0=" + t0 + " HTTP/1.1";
    client.println(getString);
    getString = "Host: ";
    client.println(getString + server);
    client.println("Connection: close");
    client.println();

    return response();
  } else {
    if (!connectionError) {
      fileCount++; // Crear un archivo cada que haya un error de conexión
    }
    // if you didn't get a connection to the server:
    return false;
  }
}

//----- Enviar información de archivos al servidor -----//
bool SendFile(File dataFile) {

  if (client.connect(server, 80)) {

    // Serial.println("connected");
    // realizando una petición HTTP:
    String postString = "POST /sensores/agua?id=";
    postString = postString + ID + " HTTP/1.1";
    client.println(postString);
    postString = "Host: ";
    client.println(postString + server);
    client.println("Connection: close\r\nContent-Type: text/csv");
    postString = "Content-length:";
    client.println(postString + dataFile.size()); // Agregar 2 al tamaño en caso de usar JSON

    // Si el archivo es JSON
    //    postString = "[";
    //    client.println(postString + fileContent + "]");

    while (dataFile.available()) {
      // Si el archivo es CSV
      client.print(dataFile.read());
    }
    client.println();

    return response();
  } else {
    // if you didn't get a connection to the server:
    if (!connectionError) {
      fileCount++; // Crear un archivo cada que haya un error de conexión
    }
    return false;
  }
}

bool response() {
  char errorSwitch[] = "/-err-*"; // Código que indicará error en el servidor
  byte i = 0;
  // if there are incoming bytes available
  // from the server, read them and print them:
  while (client.connected()) {
    if (client.available()) {
      char c = client.read();
      if (c == errorSwitch[i]) {
        i++;
      }else{
        i=0;
      }
      //      Serial.print(c);
    }
  }

  // if the server's disconnected, stop the client:
  //  Serial.println();
  //  Serial.println("disconnecting.");
  client.stop();
  if (i == sizeof(errorSwitch)) {
    if (!connectionError) {
      fileCount++; // Crear un archivo cada que haya un error de conexión
    }
    return false;
  }

  return true;
}


