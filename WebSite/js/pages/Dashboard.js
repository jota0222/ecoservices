﻿// Progressbar
var progressBars = $(".progress .progress-bar");
if (progressBars[0]) {
    progressBars.progressbar();
    progressBars.each(function (i, bar) {
        var units = $(bar).attr('data-units');
        if (!units) {
            units = '%'
        }
        var progress = $(bar).attr('data-transitiongoal');
        $(bar).attr('title', progress + " " + units);
        $(bar).text(progress + " " + units);
    });
}
// Progressbar

// iCheck
$(document).ready(function () {
    if ($("input.flat")[0]) {
        $(document).ready(function () {
            $('input.flat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });
    }
});
// /iCheck

//<!-- gauge.js -->
//var opts = {
//    lines: 12,
//    angle: 0,
//    lineWidth: 0.4,
//    pointer: {
//        length: 0.75,
//        strokeWidth: 0.042,
//        color: '#1D212A'
//    },
//    limitMax: 'false',
//    colorStart: '#1ABC9C',
//    colorStop: '#1ABC9C',
//    strokeColor: '#F0F3F3',
//    generateGradient: true
//};
//var target = document.getElementById('foo'),
//    gauge = new Gauge(target).setOptions(opts);

//gauge.maxValue = 6000;
//gauge.animationSpeed = 32;
//gauge.set(3200);
//gauge.setTextField(document.getElementById("gauge-text"));

//<!-- /gauge.js -->

//<!-- Doughnut Chart -->
$(document).ready(function () {
    var options = {
        legend: false,
        responsive: false
    };

    new Chart(document.getElementById("canvas1"), {
        type: 'doughnut',
        tooltipFillColor: "rgba(51, 51, 51, 0.55)",
        data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
                data: [30, 10, 20],
                backgroundColor: [
                  "#3498DB",
                  "#92D050",
                  "#F39C12",
                ],
                hoverBackgroundColor: [
                  "#3498DB",
                  "#92D050",
                  "#F39C12",
                ]
            }]
        },
        options: options
    });
});
//<!-- /Doughnut Chart -->

//<!-- Flot -->
$(document).ready(function () {

    var theme = {
        color: [
            '#3498DB', '#F39C12', '#92D050', '#3498DB',
            '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
        ],

        title: {
            itemGap: 8,
            textStyle: {
                fontWeight: 'normal',
                color: '#92D050'
            }
        },

        dataRange: {
            color: ['#1f610a', '#97b58d']
        },

        toolbox: {
            color: ['#408829', '#408829', '#408829', '#408829']
        },

        tooltip: {
            backgroundColor: 'rgba(0,0,0,0.5)',
            axisPointer: {
                type: 'line',
                lineStyle: {
                    color: '#408829',
                    type: 'dashed'
                },
                crossStyle: {
                    color: '#408829'
                },
                shadowStyle: {
                    color: 'rgba(200,200,200,0.3)'
                }
            }
        },

        dataZoom: {
            dataBackgroundColor: '#eee',
            fillerColor: 'rgba(64,136,41,0.2)',
            handleColor: '#408829'
        },
        grid: {
            borderWidth: 0
        },

        categoryAxis: {
            axisLine: {
                lineStyle: {
                    color: '#408829'
                }
            },
            splitLine: {
                lineStyle: {
                    color: ['#eee']
                }
            }
        },

        valueAxis: {
            axisLine: {
                lineStyle: {
                    color: '#408829'
                }
            },
            splitArea: {
                show: true,
                areaStyle: {
                    color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
                }
            },
            splitLine: {
                lineStyle: {
                    color: ['#eee']
                }
            }
        },
        timeline: {
            lineStyle: {
                color: '#408829'
            },
            controlStyle: {
                normal: { color: '#408829' },
                emphasis: { color: '#408829' }
            }
        },

        k: {
            itemStyle: {
                normal: {
                    color: '#68a54a',
                    color0: '#a9cba2',
                    lineStyle: {
                        width: 1,
                        color: '#408829',
                        color0: '#86b379'
                    }
                }
            }
        },
        map: {
            itemStyle: {
                normal: {
                    areaStyle: {
                        color: '#ddd'
                    },
                    label: {
                        textStyle: {
                            color: '#c12e34'
                        }
                    }
                },
                emphasis: {
                    areaStyle: {
                        color: '#99d2dd'
                    },
                    label: {
                        textStyle: {
                            color: '#c12e34'
                        }
                    }
                }
            }
        },
        force: {
            itemStyle: {
                normal: {
                    linkStyle: {
                        strokeColor: '#408829'
                    }
                }
            }
        },
        chord: {
            padding: 4,
            itemStyle: {
                normal: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                },
                emphasis: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    },
                    chordStyle: {
                        lineStyle: {
                            width: 1,
                            color: 'rgba(128, 128, 128, 0.5)'
                        }
                    }
                }
            }
        },
        gauge: {
            startAngle: 225,
            endAngle: -45,
            axisLine: {
                show: true,
                lineStyle: {
                    color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                    width: 8
                }
            },
            axisTick: {
                splitNumber: 10,
                length: 12,
                lineStyle: {
                    color: 'auto'
                }
            },
            axisLabel: {
                textStyle: {
                    color: 'auto'
                }
            },
            splitLine: {
                length: 18,
                lineStyle: {
                    color: 'auto'
                }
            },
            pointer: {
                length: '90%',
                color: 'auto'
            },
            title: {
                textStyle: {
                    color: '#333'
                }
            },
            detail: {
                textStyle: {
                    color: 'auto'
                }
            }
        },
        textStyle: {
            fontFamily: 'Arial, Verdana, sans-serif'
        }
    };

    var echartBar = echarts.init(document.getElementById('mainb'), theme);

    echartBar.setOption({
        //title: {
        //    text: 'Movimiento Últimos 12 Días',
        //    subtext: 'Aqui puedes ver el consumo en cada uno de tus servicios'
        //},
        tooltip: {
            trigger: 'axis'
        },
        legend: {
            data: ['agua', 'energía', 'gas']
        },
        toolbox: {
            show: false
        },
        calculable: false,
        xAxis: [{
            type: 'category',
            data: ['Mie', 'Jue', 'Vie', 'Sab', 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab', 'Dom']
        }],
        yAxis: [{
            type: 'value'
        }],
        series: [{
            name: 'agua',
            type: 'bar',
            data: [9000, 8000, 5000, 4800, 8150, 7394, 7908, 8110, 6980, 7750, 7000, 8070],
            markPoint: {
                data: [{
                    type: 'max',
                    name: '$'
                }, {
                    type: 'min',
                    name: '$'
                }]
            },
            markLine: {
                data: [{
                    type: 'average',
                    name: '$'
                }]
            }
        }, {
            name: 'energía',
            type: 'bar',
            data: [7100, 9100, 4090, 4100, 8850, 3942, 1345, 1050, 1150, 2056, 3003, 7204],
            markPoint: {
                data: [{
                    type: 'max',
                    name: '$'
                }, {
                    type: 'min',
                    name: '$'
                }]
            },
            markLine: {
                data: [{
                    type: 'average',
                    name: '$'
                }]
            }
        }, {
            name: 'gas',
            type: 'bar',
            data: [10050, 11000, 5000, 7839, 4081, 5782, 9283, 11747, 8273, 2352, 2478, 3513],
            markPoint: {
                data: [{
                    type: 'max',
                    name: '$'
                }, {
                    type: 'min',
                    name: '$'
                }]
            },
            markLine: {
                data: [{
                    type: 'average',
                    name: '$'
                }]
            }
        }]
    });

    $(window).on('resize', function () {
        if (echartBar != null && echartBar != undefined) {
            echartBar.resize();
        }
    });

});

//<!-- /Flot -->
//<!-- JQVMap -->

$(document).ready(function () {
    $('#world-map-gdp').vectorMap({
        map: 'world_en',
        backgroundColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#666666',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: ['#E6F2F0', '#149B7E'],
        normalizeFunction: 'polynomial'
    });
});

//<!-- /JQVMap -->
//<!-- Skycons -->

$(document).ready(function () {
    var icons = new Skycons({
        "color": "#73879C"
    }),
      list = [
        "clear-day", "clear-night", "partly-cloudy-day",
        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
        "fog"
      ],
      i;

    for (i = list.length; i--;)
        icons.set(list[i], list[i]);

    icons.play();
});

//<!-- /Skycons -->

//<!-- bootstrap-daterangepicker -->

$(document).ready(function () {

    var cb = function (start, end, label) {
        console.log(start.toISOString(), end.toISOString(), label);
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };

    var optionSet1 = {
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        maxDate: '12/31/2015',
        dateLimit: {
            days: 60
        },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM/DD/YYYY',
        separator: ' to ',
        locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
        }
    };
    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange').daterangepicker(optionSet1, cb);
    $('#reportrange').on('show.daterangepicker', function () {
        console.log("show event fired");
    });
    $('#reportrange').on('hide.daterangepicker', function () {
        console.log("hide event fired");
    });
    $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
        console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
        console.log("cancel event fired");
    });
    $('#options1').click(function () {
        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });
    $('#options2').click(function () {
        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });
    $('#destroy').click(function () {
        $('#reportrange').data('daterangepicker').remove();
    });
});

//<!-- /bootstrap-daterangepicker -->