﻿$(document).ready(function () {
    var data1 = [
        [gd(2017, 1, 1), 10000],
        [gd(2017, 2, 1), 16097],
        [gd(2017, 3, 1), 15400],
        [gd(2017, 4, 1), 12688],
        [gd(2017, 5, 1), 17957],
        [gd(2017, 6, 1), 15990],
        [gd(2017, 7, 1), 11867]
    ];

    var data2 = [
        [gd(2017, 1, 1), 11855],
        [gd(2017, 2, 1), 10096],
        [gd(2017, 3, 1), 15879],
        [gd(2017, 4, 1), 13000],
        [gd(2017, 5, 1), 12000],
        [gd(2017, 6, 1), 14500],
        [gd(2017, 7, 1), 13800]
    ];

    var data3 = [
        [gd(2017, 1, 1), 14000],
        [gd(2017, 2, 1), 13000],
        [gd(2017, 3, 1), 14800],
        [gd(2017, 4, 1), 9800],
        [gd(2017, 5, 1), 7500],
        [gd(2017, 6, 1), 14300],
        [gd(2017, 7, 1), 15200]
    ];

    $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
      data1, data2, data3
    ], {
        series: {
            lines: {
                show: false,
                fill: false
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.1
            },
            points: {
                radius: 2,
                show: true
            },
            shadowSize: 2
        },
        grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
        },
        colors: ["rgb(52, 152, 219)", "rgb(146, 208, 80)", "rgb(243, 156, 18)"],
        xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "month"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: false
    });

    // Textos sobre los puntos seleccionados

    $("<div id='tooltip'></div>").css({
        position: "absolute",
        display: "none",
        border: "1px solid #fdd",
        padding: "2px",
        "background-color": "#fee",
        opacity: 0.80
    }).appendTo("body");

    $("#canvas_dahs").bind("plothover", function (event, pos, item) {


        var str = "(" + pos.x.toFixed(2) + ", " + pos.y.toFixed(2) + ")";
        $("#hoverdata").text(str);

        if (item) {
            var x = item.datapoint[0].toFixed(2),
                y = item.datapoint[1].toFixed(2);

            $("#tooltip").html('$' + parseInt(y, 0))
                .css({ top: item.pageY + 5, left: item.pageX + 5 })
                .fadeIn(200);
        } else {
            $("#tooltip").hide();
        }

    });


    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
    }
});
