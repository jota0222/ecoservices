angular.module("ecoServices", ["ngRoute"])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {     
                templateUrl: 'views/dashboard/index.html',
                controller: 'DashboardController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/ConsejosAgua', {   
                templateUrl: 'views/consejos/consejoagua.html',
                controller: 'WaterTipsController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/ConsejosGas', {
                templateUrl: 'views/consejos/consejogas.html',
                controller: 'GasTipsController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/ConsejosEnergia', {
                templateUrl: 'views/consejos/consejoenergia.html',
                controller: 'EnergyTipsController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/Consejos', {
                templateUrl: 'views/consejos/consejogeneral.html',
                controller: 'GeneralTipsController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/PQRS', {
                templateUrl: 'views/about/pqr.html',
                controller: 'PQRController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/Soporte', {
                templateUrl: 'views/about/soporte.html',
                controller: 'SupportController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/Ahorro', {
                templateUrl: 'views/ahorro/index.html',
                controller: 'SavingController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/HistorialConsumo', {
                templateUrl: 'views/historialConsumo/index.html',
                controller: 'HistorialController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            //.when('/FechasPago', {
            //    templateUrl: 'views/fechaPago/index.html',
            //    controller: 'PayDayController',
            //    //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
            //    //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            //})
            .when('/FechasOtros', {
                templateUrl: 'views/blank.html',
                controller: 'SavingController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/Proposito', {
                templateUrl: 'views/proposito/index.html',
                controller: 'PurposeController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/HistorialPropositos', {
                templateUrl: 'views/blank.html',
                controller: 'SavingController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/ComercioElectronico', {
                templateUrl: 'views/blank.html',
                controller: 'SavingController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .when('/Pagos', {
                templateUrl: 'views/pagos/index.html',
                controller: 'PagosController',
                //controllerAs: 'xxxxxxCtrl',    // Si es necesario, se le asigna una variable al controlador
                //requiresLogin: true     // Ejecutar restricciones sobre esta vista 
            })
            .otherwise({
                templateUrl: 'assets/templates/home/lose.html', // Tamplate de p�gina inexistente
                controller: 'BaseController',
                //    controllerAs: 'Ctrl'
            });
    }
);