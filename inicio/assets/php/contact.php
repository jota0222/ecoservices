<?php
require("sendgrid/sendgrid-php.php");

header('Content-type: application/json');

if($_POST)
{
    $to_email       = "hover.martinez@upb.edu.co"; //Recipient email, Replace with own email here
   
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
       
        $output = json_encode(array( //create JSON data
            'type'=>'error',
            'text' => 'La petición debe ser por Ajax POST'
        ));
        die($output); //exit script outputting json data
    }

    //Sanitize input data using PHP filter_var().
    $user_name      = filter_var($_POST["name"], FILTER_SANITIZE_STRING);
    $user_email     = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
    $phone_number   = filter_var($_POST["phone"], FILTER_SANITIZE_NUMBER_INT);
    $topic          = filter_var($_POST["topic"], FILTER_SANITIZE_STRING);
    $message        = filter_var($_POST["message"], FILTER_SANITIZE_STRING);

    //additional php validation
    if(strlen($user_name)<4){ // If length is less than 4 it will output JSON error.
        $output = json_encode(array('type'=>'error', 'text' => '¡El nombre es muy corto o está vacío!'));
        die($output);
    }

    if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)){ //email validation
        $output = json_encode(array('type'=>'error', 'text' => '¡Por favor ingrese un email válido!'));
        die($output);
    }


    if(!filter_var($phone_number, FILTER_SANITIZE_NUMBER_FLOAT)){ //check for valid numbers in phone number field
        $output = json_encode(array('type'=>'error', 'text' => 'Ingrese solo dígitos en el campo de teléfono'));
        die($output);
    }

    if(strlen($message)<3){ //check emtpy message
        $output = json_encode(array('type'=>'error', 'text' => '¡Mensaje muy corto! Por favor agregue algo.'));
        die($output);
    }

    //email subject
    $subject ='Razón de contacto: '.$topic;

    //email body
    $message_body = $message."\r\n\r\n-".$user_name."\r\n\r\nEmail : ".$user_email."\r\nTeléfono : ". $phone_number ;
	
	
	$sgFrom = new SendGrid\Email(null, $user_email);
	$sgTo = new SendGrid\Email(null, $to_email);
	$sgContent = new SendGrid\Content("text/plain", $message_body);
	$sgEmail = new SendGrid\Mail($sgFrom, $subject, $sgTo, $sgContent);
	
	$sendgrid = new SendGrid("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");
	
	$response = $sendgrid->client->mail()->send()->post($sgEmail);
	
	if ($response->statusCode()==202){
		$output = json_encode(array('type'=>'success', 'text' => 'Hola '.$user_name .', gracias por el mensaje, te estaremos respondiendo pronto.'));
	} else {
		$output = json_encode(array('type'=>'error', 'text' => '¡No se puede enviar el correo! Intentalo más tarde.'));
	}
	die($output);
}


?>
